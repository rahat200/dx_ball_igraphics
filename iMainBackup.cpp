# include "iGraphics.h"
#include<stdlib.h>
#include<windows.h>
/* **********************************
*     Tamjid al Rahat                               *
************************************
*/
	
int i,j,delay=200;
int row=6;
int column=10;
int brickw=75;
int brickh=30;
int brickx[6][10];
int bricky[6][10];
int valid[6][10];
int winw=800;
int winh=600;
int radius=10;
int dx=1,dy=1;
int ballstart=1;
int boxx=winw/2-90,boxy=5,boxw=180,boxh=17;
int ballx=boxx+boxw/2;
int bally=boxy+boxh+radius+2;
int brickline;
int ballhitbox=0;
int boxsidehit=0;
int levelflag=0;
int levelcount=1,turn=0;
int movebox,xbefore;
int nextlevel=0;
int lostcount=0;
int gameover=0;
int start=1;
int help=1;
int score=0,sdx=3;
int lifetime=3;
int step;
int resume=0;
int begin=1;
int credit=0;
int temp=20,x=10;//variable for loading
int gamestart=0,continu=0,newgame=0;
int sure=0,yes=0,no=0;
int bonuslist[6];
int hscore=0;
char hscorech[10];
void checkbrickhit();
void bonus(void);
int highscore();
void savegame();
void loadgame();

void mydelay(int milliseconds)
{
	for(int i=0;i<milliseconds*1000;i++);
}

void createbricks()
{
	int x=5;
	int y=570;
	
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			brickx[i][j]=x;
			bricky[i][j]=y;
			if(newgame==1)
				valid[i][j]=1;
			x=x+brickw+5;
		}
		x=5;
		y=y-brickh-5;
	}	
}
void brickdraw()
{
	int i,j;
	
	//iSetcolor(43.0/250,96.0/250,196.0/250);
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			if(j==bonuslist[i]&&valid[i][j]==1&&!(i%2))
			{
				iShowBMP(brickx[i][j],bricky[i][j],"bonus1.bmp");
			}
			else if(valid[i][j]==1)	
			{
				iSetcolor(12.0/250,214.0/250,209.0/250);
				iFilledRectangle(brickx[i][j],bricky[i][j],brickw,brickh);
				iSetcolor(152.0/250,12.0/250,87.0/250);
				iFilledRectangle(brickx[i][j]+10,bricky[i][j]+5,brickw-20,brickh-10);
				iSetcolor(0.0/250,0.0/250,0.0/250);
				iFilledEllipse(brickx[i][j]+brickw/2,bricky[i][j]+brickh/2,15,5);
				
			}
		}
	}
}

void balldraw()
{
	iSetcolor(0,250,0);
	iFilledCircle(ballx,bally,radius);
}

void boxhitball()
{
	if(bally-radius==boxy+boxh  && boxx-10<=ballx && ballx<=boxx+boxw+10)
	{
		ballhitbox=1; //boxupperhit
		PlaySound("boxhit.wav", NULL, SND_ASYNC);
	}
	else if((bally<=boxy+boxh&&ballx-radius<=boxx+boxw&&ballx>boxx&&boxy<=bally-radius)||(bally<=boxy+boxh&&ballx+radius>=boxx&&ballx<boxx+boxw&&boxy<=bally-radius))
		boxsidehit=1;    //boxsidehit
	else 
	{
		ballhitbox=0;
		boxsidehit=0;
	}
		
}

void ballmove()
{
	//printf("%d=%d\n",dx,dy);
	mydelay(delay);
	boxhitball();
	checkbrickhit();
	if(ballhitbox==1)
	{
		
		if(step>2&&dx<0)
		{
			dx=-2;
			dx=-dx;
			dy=-dy;
		}
		else if(step>2&&dx>0)
		{
			dy=-dy;
			dx=2;
		}
		else if(step<-2&&dx>0)
		{
			dx=2;
			dx=-dx;
			dy=-dy;
		}
		else if(step<-2&&dx<0)
		{
			dy=-dy;
			dx=-2;
		}
		else
		{
			dy=-dy;
		}
	}
	else if(boxsidehit==1)
	{
		dx=-dx;
		boxsidehit=0;
	}
	  else if(turn==0||ballhitbox==0)
	{
		//mydelay(10);
		if(ballx+radius>=winw)
				dx=-dx;
		else if(bally+radius>=winh)
				dy=-dy;
		else if(ballx-radius<=0)
				dx=-dx;
		else if(bally-radius<=0)
		{
			lifetime--;
			PlaySound("fail.wav", NULL, SND_ASYNC);
			if(lifetime==0)
			{
				savegame();
				highscore();
				gameover=1;
				lostcount=0;
				levelcount=1;
				delay=200;
				newgame=0;
				continu=0;
				score=0;
				lifetime=3;
				sdx=3;
				
			}
				ballx=boxx+boxw/2;
				bally=boxy+boxh+radius+2;
				ballstart=0;
		}		
	   }
	  else 
	  {
			dx=-dx;
			dy=-dy;
	  }
			   ballx+=dx;
				bally+=dy;
}

void checkbrickhit()
{
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			if(valid[i][j]==1&&(bally+radius>=bricky[i][j]&&bally+radius<bricky[i][j]+brickh&&brickx[i][j]<=ballx&&brickx[i][j]+brickw>=ballx))
			{
				valid[i][j]=0;
				if(dx==2) dx=1;
				else if(dx==-2) dx=-1;
				dy=-dy;		                                            //brickdownhit
				score+=sdx;
				PlaySound("bottle.wav", NULL, SND_ASYNC);
				if(j==bonuslist[i]&&!(i%2))
				{
					lifetime++;
					PlaySound("bonus.wav", NULL, SND_ASYNC);
				}
			}
			else if(valid[i][j]==1&&(bally<=bricky[i][j]+brickh&&bally>=bricky[i][j]&&ballx+radius>=brickx[i][j]&&ballx<=brickx[i][j]+brickw))
			{
				valid[i][j]=0;
				if(dx==2) dx=1;
				else if(dx==-2) dx=-1;
				dx=-dx;			                                           //bricklefthit
				score+=sdx;
				PlaySound("bottle.wav", NULL, SND_ASYNC);
				if(j==bonuslist[i]&&!(i%2))
				{
					lifetime++;
					PlaySound("bonus.wav", NULL, SND_ASYNC);
				}
			}
			else if(valid[i][j]==1&&(ballx-radius<=brickx[i][j]+brickw&&bricky[i][j]<=bally&&bricky[i][j]+brickh>=bally&&brickx[i][j]<ballx-radius))
			{
				valid[i][j]=0;
				if(dx==2) dx=1;
				else if(dx==-2) dx=-1;
				dx=-dx;			                                        //brickrighthit
				score+=sdx;
				PlaySound("bottle.wav", NULL, SND_ASYNC);
				if(j==bonuslist[i]&&!(i%2))
				{
					lifetime++;
					PlaySound("bonus.wav", NULL, SND_ASYNC);
				}
			}	
			else if(valid[i][j]==1&&bally-radius<=bricky[i][j]+brickh&&brickx[i][j]<=ballx&&brickx[i][j]+brickw>=ballx&&bally-radius>bricky[i][j])
			{
				valid[i][j]=0;
				if(dx==2) dx=1;
				else if(dx==-2) dx=-1;
				dy=-dy;		                                      //brickupperhit
				score+=sdx;
				PlaySound("bottle.wav", NULL, SND_ASYNC);
				if(j==bonuslist[i]&&!(i%2))
				{
					lifetime++;
					PlaySound("bonus.wav", NULL, SND_ASYNC);
				}
			}
		}
	}
}
void levelcomplete()
{
	for(i=0;i<row;i++)
	{
		for(j=0;j<column;j++)
		{
			if(valid[i][j]==1)
				return;
		}
	}
PlaySound("level.wav", NULL, SND_ASYNC);
	levelflag=1;
	if(nextlevel==1)
	{
			for(i=0;i<row;i++)
			{
				for(j=0;j<column;j++)
				{
					valid[i][j]=1;
				}
			}
			ballx=boxx+boxw/2;
			bally=boxy+boxh+radius+2;
			delay=delay-30;
			//boxw=boxw-15;
			ballstart=0;
			nextlevel=0;
			levelflag=0;
			sdx+=2;
			levelcount++;
			bonus();
	}
}
void bonus(void)
{
	int i;
	for(i=0;i<row;i++)
	{
		bonuslist[i]=rand()%10;
	}
}
int highscore()
{
	int highscore;
	int temp;
	
	FILE *fp;
	fp=fopen("highscore.txt","r");
		fscanf(fp,"%d",&highscore);
	temp=highscore;
	fclose(fp);
	fp=fopen("highscore.txt","w");
	
	if(score>highscore)
	{
		fprintf(fp,"%d",score);
		fclose(fp);
		return score;
	}
	else
	{
		fprintf(fp,"%d",temp);
		fclose(fp);
		return temp;
	}
	
}
void savegame()
{
	FILE *fp;
	fp=fopen("savegame.txt","w");
	for(i=0;i<row;i++)
			{
				for(j=0;j<column;j++)
				{
					fprintf(fp,"%d ",valid[i][j]);
				}
			}
	fprintf(fp,"\n");
	fprintf(fp,"%d %d %d",levelcount,lifetime,score);
	fclose(fp);

}
void loadgame()
{
	FILE *fp;
	fp=fopen("savegame.txt","r");
	for(i=0;i<row;i++)
			{
				for(j=0;j<column;j++)
				{
					fscanf(fp,"%d ",&valid[i][j]);
				}
			}
	//fprintf(fp,"\n");
	fscanf(fp,"%d %d %d",&levelcount,&lifetime,&score);
	fclose(fp);
}

void iDraw()
{
	//mydelay(delay);
	 if(begin==1)
	{
		iShowBMP(0,0,"start1.bmp");
		iSetcolor(173.0/250,173.0/250,173.0/250);
		iText(200,100,"Loading ",GLUT_BITMAP_TIMES_ROMAN_24);
		iSetcolor(0.0/250,176.0/250,80.0/250);
		iRectangle(300,100,300,12);
		iRectangle(298,98,304,16);
		iSetcolor(233.0/250,41.0/250,9.0/250);
		mydelay(40000);
		iFilledRectangle(301,102,x,10);
		x=x+temp;
		if(x>=300)
		{
			credit=1;
			begin=0;
			x=10;
			temp=20;
		}
	}
	else if(newgame==1)
	{
		iClear();
		createbricks();
		iSetcolor(255.0/250,173.0/250,173.0/250);
		iText(200,300,"Loading ",GLUT_BITMAP_TIMES_ROMAN_24);
		iSetcolor(0.0/250,176.0/250,80.0/250);
		iRectangle(300,300,300,12);
		iRectangle(298,298,304,16);
		iSetcolor(255.0/250,0.0/250,9.0/250);
		mydelay(50000);
		iFilledRectangle(301,302,x,9);
		x=x+temp;
		if(x>=300)
		{
			newgame=0;	
			ballx=boxx+boxw/2;
			bally=boxy+boxh+radius+3;
			ballstart=0;//printf("play\n");
			score=0;
			lifetime=3;
			levelcount=1;
			gamestart=0;
			x=10;
			temp=20;
		}
		
	}
	else if(hscore==1)
	{
		iClear();
		sprintf(hscorech,"%d",highscore());
		iSetcolor(172.0/250,1.0/250,28.0/250);
		iText(270,310,"HIGHSCORE  ",GLUT_BITMAP_HELVETICA_18);
		iText(400,310,hscorech,GLUT_BITMAP_HELVETICA_18);
	}
	else if(gamestart==1)
	{
		iShowBMP(0,0,"newgame.bmp");
	}
	else if(credit==1)
	{
		iShowBMP(0,0,"credit.bmp");
	}
	else if(levelcount==6)
	{
		iText(400,300,"YOU WIN !!! ",GLUT_BITMAP_HELVETICA_18);
	}
	else if(levelflag==1)
	{	
		iShowBMP(0,0,"levelcomplete.bmp");
		if(sure==1)//level quit sure
		{
			
			iShowBMP(100,150,"sure.bmp");
			if(yes==1)
			{
				highscore();
				savegame();
				exit(0);
			}
			else if(no==1)
			{
				//resume=0;
				sure=0;
				no=0;
			}
		}
	}
	else if(start==1&&help==1)
	{
		iShowBMP(0,0,"menu.bmp");
	}
	else if(help==0)
	{
			iShowBMP(0,0,"help.bmp");
	}
	else if(gameover==1)
	{
		iShowBMP(0,0,"gameover.bmp");
	}
	else 
	{
		iClear();
		iShowBMP(boxx,boxy,"box.bmp");
		checkbrickhit();
		brickdraw();
		levelcomplete();
		balldraw();
		 if(ballstart==0)
		 {
			 iSetcolor(250.0/250,250.0/250,250.0/250);
			iText(300,200,"Press ' s ' to start ",GLUT_BITMAP_TIMES_ROMAN_24);
		 }
		if(ballstart==1&&resume==0)
			ballmove();
		
		iSetcolor(220.0/250,213.0/250,220.0/250);
		iFilledRectangle(0,winh,winw,30);
		char levelch[10],scorech[20],lifetimech[10];
		sprintf(levelch,"%d",levelcount);
		sprintf(scorech,"%d",score);
		sprintf(lifetimech,"%d",lifetime);
		iSetcolor(172.0/250,1.0/250,28.0/250);
		iText(20,610,"LEVEL ",GLUT_BITMAP_HELVETICA_18);
		iText(90,610,levelch,GLUT_BITMAP_HELVETICA_18);
		iText(680,610,"SCORE ",GLUT_BITMAP_HELVETICA_18);
		iText(760,610,scorech,GLUT_BITMAP_HELVETICA_18);
		iText(350,610,"LIFETIME ",GLUT_BITMAP_HELVETICA_18);
		iText(450,610,lifetimech,GLUT_BITMAP_HELVETICA_18);
		if(sure==1)
		{
			
			iShowBMP(100,150,"sure.bmp");
			if(yes==1)
			{
				highscore();
				savegame();
				exit(0);
			}
			else if(no==1)
			{
				resume=0;
				sure=0;
				no=0;
			}
		}
	}

}
/*
	function iMouseMove() is called when the user presses and drags the mouse.
	(mx, my) is the position where the mouse pointer is.
*/

void iMouseMove(int mx, int my)
{
	
	turn++;
	if(movebox==1)
	{
		step=mx-xbefore;
		xbefore=mx;
		if(boxx+step+boxw<=winw&&boxx+step>=0)
		{
			boxx = boxx + step;
			if(ballstart==0)
				ballx = ballx + step;
		}
	
	}
		
}

/*
	function iMouse() is called when the user presses/releases the mouse.
	(mx, my) is the position where the mouse pointer is.
*/
void iMouse(int button, int state, int mx, int my)
{
	 //printf("%d=%d\n",mx,my);
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		if(levelcount==6&&mx&&my)
			exit(0);
		else if(sure==1&&mx>=113&&mx<=235&&my>=287&&my<=365)
			yes=1;
		else if(sure==1&&mx>=285&&mx<=419&&my>=289&&my<=365)
			no=1;
		else if(hscore==1&&mx>=0&&mx<=800&&my>=0&&my<=600)
			hscore=0;
		else if(credit==1&&mx>=0&&mx<=800&&my>=0&&my<=600)
		{
			credit=0;
			start=1;
			help=1;//menu
		}
		else if(help==0&&mx>=23&&mx<=159&&my>=8&&my<=128)
		{
			start=1;
			help=1;
		}
		else if(start==1&&help==1&&mx>=620&&mx<=745&&my>=460&&my<=590)
		{
			start=0;
			gamestart=1;
		}
		else if(gamestart==1&&continu==0&&mx>=260&&mx<=520&&my>=420&&my<=485)
		{
			continu=1;        //load game
			gamestart=0;
			loadgame();
			if(lifetime==0)
			{
				start=1;
				help=1;
				continu=0;
			}
			ballx=boxx+boxw/2;
			bally=boxy+boxh+radius+3;
			ballstart=0;
		}
		else if(gamestart==1&&newgame==0&&mx>=260&&mx<=520&&my>=291&&my<=352)
		{
			newgame=1;//newgame start
		}
		else if(gamestart==1&&mx>=260&&mx<=544&&my>=155&&my<=220)
		{
			hscore=1;//highscore
		}

		else if(start==1&&help==1&&mx>=630&&mx<=750&&my>=30&&my<=150)
			exit(0); //quit
		else if(help==1&&mx>=623&&mx<=742&&mx<=742&&my>=255&&my<=375)
		{
			help=0;//printf("help\n");
		}
		if( boxx<=mx && mx<=boxx+boxw && boxy<=my && my<=boxy+boxh+100)
		{
			movebox=1;
			xbefore=mx;
		}
	
		if(levelflag==1&&mx>=357&&mx<=782&&my>=35&&my<=154)
		{
			nextlevel=1;
			levelcomplete();//continue
		}
		else if(levelflag==1&&mx>=13&&mx<=146&&my>=13&&my<=160)
		{
			//resume=1;
			sure=1;
		}
		if(gameover==1&&mx>=631&&mx<=751&&my>=262&&my<=500)
		{
			start=1;
			help=1;
			gameover=0;//mainmenu
			///////createbricks();
			bonus();
		}
		else if(gameover==1&&mx>=20&&mx<=125&&my>=36&&my<=148)
			exit(0);
	}
	else if(button == GLUT_LEFT_BUTTON && state == GLUT_UP)
	{
		movebox=0;
	}
	else if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		//place your codes here
	}
}

/*
	function iKeyboard() is called whenever the user hits a key in keyboard.
	key- holds the ASCII value of the key pressed.
*/
void iKeyboard(unsigned char key)
{
	if(key == 'q'&&sure==0)
	{
		
		//exit(0);
		resume=1;
		sure=1;
	}
	if(key=='a')
	{
		if(boxx-10>=0)
		{
			boxx=boxx-10;
			if(ballstart==0)
				ballx=ballx-10;
		}
	}
	if(key=='l')
	{
		if((boxx+10+boxw)<=winw)
		{
			boxx=boxx+10;
			if(ballstart==0)
				ballx=ballx+10;
		}
	}
	if(key=='s')
	{
		ballmove();
		ballstart=1;	
	}
	if(key=='n')
		levelflag=0;
	if(key=='m'&&help==0)
	{
		help=1;
		start=1;
	}
	if(key=='p')
	{
		start=0;
		ballx=boxx+boxw/2;
		bally=boxy+boxh+radius+2;
		ballstart=0;//printf("play\n");
	}
	if(resume==0&&key=='r')
		resume=1;
	else if(resume==1&&key=='r')
		resume=0;

	//place your codes for other keys here
}

int main()
{
	createbricks();
	bonus();
	if(start==1&&help==1)
	PlaySound("begin2.wav", NULL, SND_ASYNC);
	iInitialize(winw,winh+30, "dx ball");
	return 0;
}
